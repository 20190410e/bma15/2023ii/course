package pe.edu.uni.msalasy.tv;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class TestTV {

    public static void main(String[] args) {
        System.out.println("TEST TV!");
        TV tv1 = new TV();
        tv1.turnOn();
        System.out.println(tv1.toString());
        if (tv1.isOn()) {
            System.out.println("Encendido");
            System.out.println("Colocando un canal");
            int newChannel = 2;
            tv1.setChannel(newChannel);
            int newvolLevel = 5;
            tv1.setVolume(newvolLevel);
            System.out.println(tv1.toString());
            tv1.channelDown();
            System.out.println(tv1.toString());
            tv1.volumeUp();
            System.out.println(tv1.toString());
            tv1.channelDown();
            System.out.println(tv1.toString());
            tv1.volumeUp();
            System.out.println(tv1.toString());
            tv1.volumeUp();
            System.out.println(tv1.toString());
            tv1.volumeDown();
            System.out.println(tv1.toString());
        } else {
            System.out.println("Apagado");
        }

    }
}
