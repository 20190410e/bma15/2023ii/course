package pe.edu.uni.msalasy.tv;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class TV {

    int channel;
    int volumeLevel;
    boolean on;
    
    //GETTER, los getters hacen una sola cosa
    public boolean isOn() {
        return on;
    }

    //Limites
    final int minvolumeLevel = 1;
    final int maxvolumeLevel = 7;
    final int minchannel = 1;
    final int maxchannel = 120;

    public TV() {
        channel = 1;
        volumeLevel = 7;
        on = false;

    }

    public void turnOn() {
        this.on = true;
    }

    public void turnOff() {
        this.on = false;
    }

    public void setChannel(int newChannel) {
        this.channel = newChannel;
    }

    public void setVolume(int newVolumeLevel) {
        this.volumeLevel = newVolumeLevel;
    }

    public void channelUp() {
        if (on && channel < maxchannel) {
            channel++;
        } else if (channel == maxchannel) {
            channel = minchannel;
        }
    }

    public void channelDown() {
        if (on && minchannel < channel) {
            channel--;
        } else if (channel == minchannel) {
            channel = maxchannel;
        }
    }

    public void volumeUp() {
        if (on && volumeLevel < maxvolumeLevel) {
            volumeLevel++;
        }
    }

    public void volumeDown() {
        if (on && minvolumeLevel < volumeLevel) {
            volumeLevel--;
        }
    }

    @Override
    public String toString() {
        return "TV{" + "channel=" + channel + ", volumeLevel=" + volumeLevel + ", on=" + on + '}';
    }

}
