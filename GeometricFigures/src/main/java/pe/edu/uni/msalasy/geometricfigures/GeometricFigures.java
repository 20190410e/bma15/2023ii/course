package pe.edu.uni.msalasy.geometricfigures;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class GeometricFigures {

    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(4.6d);
        Circle circle3 = new Circle(5.7d, "Blue", false);

        circle1.printCircle();
        circle2.printCircle();
        circle3.printCircle();

        Rectangle rec1 = new Rectangle();
        Rectangle rec2 = new Rectangle(2.4d,18d);
        Rectangle rec3 = new Rectangle(10,20,"Green",true);
        System.out.println("El perimetro es: " + rec1.getPerimeter());
        System.out.println("El perimetro es: " + rec2.getPerimeter());
        System.out.println("El area del tercer rectangulo es " + rec3.getArea());

    }
}
