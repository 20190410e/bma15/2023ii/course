package pe.edu.uni.msalasy.geometricfigures;

import java.util.Date;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class GeometricObject {
    private String color;
    private boolean filled;
    private java.util.Date dateCreated = new java.util.Date();
    
    public GeometricObject(){
        color = "Red";
        filled = true;
    }
    public GeometricObject(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public String toString() {
        return "GeometricObject{" + "color=" + color + ", filled=" + filled + ", dateCreated=" + dateCreated + '}';
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    
}
