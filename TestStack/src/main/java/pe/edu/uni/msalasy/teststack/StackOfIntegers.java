package pe.edu.uni.msalasy.teststack;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class StackOfIntegers {

    private int[] elements;
    private int size;

    public StackOfIntegers() {

    }

    public StackOfIntegers(int capacity) {

    }

    public boolean empty() {
        return size == 0;
    }

    public int peek() {
        return elements[size - 1];
    }

}
