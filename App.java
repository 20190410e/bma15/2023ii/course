package pe.edu.uni.msalasy.conditional;

/**
 *
 * @author mauricio thiago salas yap <mauricio.salas.y@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        int i = 35;
        if (30 < i) {
            System.out.println("el numero es mayor a 30");
        }
        if (i % 2 == 1) {
            System.out.println("es impar");
        }
        if (i % 2 == 0) {
            System.out.println("es par");
        }

        System.out.println("---------------");

        int j = 25;
        if (30 < j) {
            System.out.println("el numero es mayor a 30");
        } else {

            if (j % 2 == 1) {
                System.out.println("es impar");
            } else {
                if (j % 2 == 0) {
                    System.out.println("es par");
                }
            }
        }
        /*
        System.out.println("+++++++++++++++++++");

        int k = 33;
        if (30 < k) {
            System.out.println("el numero es mayor a 30");
            return;
        }
        if (k % 2 == 1) {
            System.out.println("es impar");
            return;
        }
        if (k % 2 == 0) {
            System.out.println("es par");
            return;
        }
         */
        int m = 11;
        switch (i % 2) {
            case 0:
                System.out.println("es par");
                break;
            case 1:
                System.out.println("es impar");
                break;
            default:
                System.out.println("cualquier otro número");
        }

    }
}
