package pe.edu.uni.msalasy.operators;

import java.io.File;

/**
 *
 * @author mauricio thiago salas yap <mauricio.salas.y@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        
        System.out.println("-");
        System.out.println("Operators!");
        System.out.println("+");
        
        int y1 = 4;
        double x1 = 3+2 * --y1;
        
        System.out.println(" " + x1);
        
        //operadores aritmeticos 
        int x2 = 2*5+3*4-8; 
        System.out.println(x2); 
        
        int x3 = 2*((5+3)*4-8); 
        System.out.println(x3); 
        
        System.out.println(10 % 3);
        System.out.println(11 / 3);
        System.out.println(12 / 3);
        System.out.println(12 % 3);
        
        int x4 = 3;
        long y4 = 32;
        System.out.println(x4*y4);
        
        System.out.println("Bytes: " + Integer.SIZE/8);
        System.out.println("Bytes: " + Long.SIZE/8);
        
        double x5 = 39.21;
        float y5 = 3.1f;
        System.out.println(x5+y5);
        
        short x6 = 10;
        short y6 = 3;
        System.out.println(x6/y6);
        
        short x7 = 17;
        float y7 = 3;
        double z7 = 38;
        System.out.println(x7*y7/z7);
        //Short to int, int to float, float to double
        //If there's arithmetic operators, promotions go first, then operations
        
        boolean _d = false;
        Boolean _e = false;
        //The first one is a primitive one, the second is a class, both are valid
        System.out.println("_d: " + _d);
        _d = !_d;
        System.out.println("_d: " + _d);
        
        double x9 = 1.21;
        System.out.println("x9: " + x9);
        x9 = -x9;
        System.out.println("x9: " + x9);
        x9 = -x9;
        System.out.println("x9: " + x9);
        //Simple negation
        
        int count = 0;
        System.out.println("counter: " + count);
        System.out.println("counter: " + ++count);
        System.out.println("counter: " + count);
        System.out.println("counter: " + count--);
        System.out.println("counter: " + count);
        //Pre and post 
        //Remeber print does nothing
        
        int x = 3;
        int y = ++x * 5 / x-- + --x;
        System.out.println("x: " + x);
        System.out.println("y: " + y);
        //Remember this part, x pre-elevates to 4 then stays at 4 and then goes down to 3, then pre-downgrades
        //to 2
       
        int a1 = (int)1.8;
        //casting
        //Problem isn't the amount of information, the problem is that this is a double
        
        short b1 = (short)192122;
        System.out.println("b1: " + b1);
        
        int c1 = (int)9f;
        System.out.println("c1: " + c1);
        
        long d1 = 432523423423423432l;
        System.out.println("d1: " + d1);
        
        short a2 = 10;
        short b2 = 3;
        short c2 = (short) (a2 * b2);
        System.out.println("c2: " + c2);
        //Remember to cast the whole result of the operation
        
        int a3 = 2, b3 = 3;
        //a3 = a3*b3; //asignacion simple
        a3 *= b3; //asignacion compuesta
        System.out.println(a3);
        
        long a4 = 18;
        int b4 = 5;
        //b4 = (int)(b4 * a4);
        b4 *= a4;
        //Casts in the asignation
        
        long a5 = 5;
        long b5 = (a5 = 3);
        System.out.println("a5: " + a5);
        System.out.println("b5: " + b5);
        
        int a6 = 10, b6 = 20, c6 = 30;
        System.out.println(a6<b6);
        System.out.println(a6<=b6);
        System.out.println(a6>=b6);
        System.out.println(a6>b6);
        //The result of comparatives is a boolean
        
        boolean a7 = true || (y<4);
        
        Object o;
        //Everything comes from this class
        o = new Object();
        if(o != null && o.hashCode() < 5){
            // do something
        }
        if(o != null & o.hashCode() < 5){
            // do something
        }
        
        int a8 = 6;
        boolean b8 = (a8 >= 6) || (++a8 <= 7);
        System.out.println("a8: " + a8);
        
        System.out.println("Equality operator");
        File p1 = new File("file.txt");
        File q1 = new File("file.txt");
        File r1 = p1;
        System.out.println("p1 == q1? " + (p1==q1));
        System.out.println("p1 == r1? " + (p1==r1));
        //p1 and q1 aren't the same because they come from different places
        
        
    }
} 
