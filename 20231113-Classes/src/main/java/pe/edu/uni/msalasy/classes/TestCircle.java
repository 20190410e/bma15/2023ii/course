package pe.edu.uni.msalasy.classes;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class TestCircle {

    public static void main(String[] args) {
        System.out.println("Test Circle!");
        Circle circle1;
        //creado la variable referencia
        circle1 = new Circle();
        //creando la clase con el constructor por defecto
        //el constructor ya tiene un valor
        System.out.println(circle1.toString());
        System.out.println("perimetro 1: " + circle1.getPerimeter());
        System.out.println("area 1: " + circle1.getArea());
        double radius2 = 25;
        Circle circle2;
        circle2 = new Circle(radius2);
        System.out.println(circle2.toString());
        System.out.println("perimetro 2: " + circle2.getPerimeter());
        System.out.println("area 2: " + circle2.getArea());

        double radius3 = 125;
        Circle circle3 = new Circle();
        circle3.setRadius(radius3);
        System.out.println(circle3.toString());
        System.out.println("perimetro 3: " + circle3.getPerimeter());
        System.out.println("area 3: " + circle3.getArea());

    }

}
