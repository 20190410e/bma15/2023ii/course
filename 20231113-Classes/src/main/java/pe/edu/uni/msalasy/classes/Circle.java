package pe.edu.uni.msalasy.classes;

/**
 *
 * @author Thiago Salas <msalasy@uni.edu.pe>
 */
public class Circle {

    double radius;

    Circle() {
        this.radius = 1;
    }

    Circle(double NewRadius) {
        this.radius = NewRadius;
    }

    double getArea() {
        return Math.PI * this.radius * this.radius;
    }

    double getPerimeter() {
        return Math.PI * 2 * this.radius;
    }

    void setRadius(double NewRadius) {
        this.radius = NewRadius;
    }

    @Override
    public String toString() {
        return "Circle{" + "radius=" + radius + '}';
    }
    
}
